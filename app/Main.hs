module Main where

import Data.List (intercalate)
import System.Environment (getArgs)
import System.Exit (exitSuccess, exitFailure)
import Lib

import qualified Data.ByteString.Char8 as C

main :: IO ()
main = getArgs >>= parse >>= output

output :: [(Maybe FilePath, NewlineCount)] -> IO ()
output = put . intercalate "\n" . foldr display []
   where put "" = exitSuccess
         put s  = putStrLn s >> exitFailure


parse :: [String] -> IO [(Maybe FilePath, NewlineCount)]
parse [] = (:[]) . (,) Nothing <$> countStdin
parse ps = mapM (\p -> (,) (Just p) <$> countFile p) ps

countStdin :: IO NewlineCount
countStdin = count <$> C.getContents

countFile :: FilePath -> IO NewlineCount
countFile = fmap count . C.readFile
