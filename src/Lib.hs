module Lib
    ( count
    , display
    , NewlineCount (NewlineCount)
    ) where

import qualified Data.ByteString.Char8 as C
import qualified Data.ByteString as BS
import Data.List (intercalate)

data NewlineCount = NewlineCount Int Int deriving (Show, Eq)

instance Monoid NewlineCount where
    mempty = NewlineCount 0 0
    NewlineCount ll cl `mappend` NewlineCount lr cr = NewlineCount (ll+lr) (cl+cr)

count :: BS.ByteString -> NewlineCount
count s = snd . C.foldr updateCount (Nothing, (count1 (C.head s))) $ s

count1 :: Char -> NewlineCount
count1 '\n' = NewlineCount 1 0
count1 _    = mempty

updateCount :: Char -> (Maybe Char, NewlineCount) -> (Maybe Char, NewlineCount)
updateCount '\r' (Just '\n', n) = (Just '\r', mconcat [n, NewlineCount 0 1])
updateCount c    (Just '\n', n) = (Just c   , mconcat [n, NewlineCount 1 0])
updateCount c    (_        , n) = (Just c   , n)

display :: (Maybe FilePath, NewlineCount) -> [String] -> [String]
display (_      , (NewlineCount 0 _)) xs = xs
display (_      , (NewlineCount _ 0)) xs = xs
display (Nothing, c                 ) xs = (displayCount c) : xs
display (Just p , c                 ) xs = mconcat [p, ": ", displayCount c] : xs

displayCount :: NewlineCount -> String
displayCount (NewlineCount l c) = intercalate " " ["Found",(show l),"LFs and",(show c),"CRLFs"]
