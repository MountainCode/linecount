# linecount

Detects any mixed line endings (CRLFs with LFs) and displays a count of each.

## Installing

1. Install [The Haskell Tool Stack](https://docs.haskellstack.org).
2. Run `stack install`

Stack will display where on your filesystem the executable was installed.

## Usage

Pipe the bytes to `linecount` or specify filenames.
