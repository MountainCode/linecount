import qualified Data.ByteString.Char8 as C
import System.FilePath
import Test.Hspec
import Lib

testDir = "test"

main :: IO ()
main = hspec $ do
  describe "Lib.count" $ do
    it "returns a zero count when there are no line endings" $ do
        count (C.pack "hello world") `shouldBe` NewlineCount 0 0
    it "returns 11 and 0 when there are 11 LFs and 0 CRLFs" $ do
        txt <- C.readFile (testDir </> "the-peace-of-wild-things.txt")
        count txt `shouldBe` NewlineCount 11 0
    it "returns 0 and 11 when there are 0 LFs and 11 CRLFs" $ do
        txt <- C.readFile (testDir </> "o-me-o-life.txt")
        count txt `shouldBe` NewlineCount 0 11
    it "returns 113 and 12 when there are 113 LFs and 12 CRLFs" $ do
        txt <- C.readFile (testDir </> "the-raven.txt")
        count txt `shouldBe` NewlineCount 113 12
    it "returns 1 and 0 when there is only an LF" $ do
        count (C.pack "\n") `shouldBe` NewlineCount 1 0
    it "returns 0 and 1 when there is only a CRLF" $ do
        count (C.pack "\r\n") `shouldBe` NewlineCount 0 1

  describe "Lib.display" $ do
    it "returns an empty string when there are no line endings" $ do
        display (Nothing, NewlineCount 0 0) [] `shouldBe` []
    it "returns an empty string when there are only LFs" $ do
        display (Nothing, NewlineCount 7 0) [] `shouldBe` []
    it "returns an empty string when there are only CRLFs" $ do
        display (Nothing, NewlineCount 0 5) [] `shouldBe` []
    it "returns the counts when there are mixed line endings" $ do
        display (Nothing, NewlineCount 8 3) [] `shouldBe` ["Found 8 LFs and 3 CRLFs"]
    it "returns an empty string when the FilePath has a value and no mixed endings" $ do
        display (Just "filename.txt", NewlineCount 12 0) [] `shouldBe` []
    it "returns the FilePath when it has a value and there are mixed endings" $ do
        display (Just "filename.txt", NewlineCount 14 1) [] `shouldBe` ["filename.txt: Found 14 LFs and 1 CRLFs"]
